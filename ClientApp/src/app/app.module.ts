import { ApiProdutoService } from './services/apiproduto.service';
import { ApiLoginService } from './services/apilogin.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AutenticacaoService } from './services/autenticacao.service';
import { ProdutosComponent } from './produtos/produtos.component';
import { ProdutoNovoComponent } from './produto-novo/produto-novo.component';
import { ProdutoEditarComponent } from './produto-editar/produto-editar.component';
import { registerLocaleData } from '@angular/common';
import localBR from '@angular/common/locales/pt';

registerLocaleData(localBR);
@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    LoginComponent,
    ProdutosComponent,
    ProdutoNovoComponent,
    ProdutoEditarComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'autenticar', component: LoginComponent, pathMatch: 'full' },
      {
        path: '',
        component: HomeComponent,
        canActivate: [AutenticacaoService],
        children: [
          { path: 'produtos', component: ProdutosComponent },
          { path: 'produto-novo', component: ProdutoNovoComponent },
          { path: 'produto-editar/:id', component: ProdutoEditarComponent },
          { path: '**', redirectTo: 'area-admin' }
        ]
      },
      { path: '', component: LoginComponent },
      { path: '**', redirectTo: '' }
    ])
  ],
  providers: [ApiLoginService, ApiProdutoService, AutenticacaoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
