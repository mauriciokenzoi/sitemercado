import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ApiLoginService } from '../services/apilogin.service';
import { Login } from '../models/Login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  public login: Login = { usuario: '', senha: ''};
  public erro: string;
  public validaUsuario = false;
  public validaSenha = false;

  constructor(private loginService: ApiLoginService, private router: Router) { }

  ngOnInit() {
    this.erro = '';
  }

    public autenticar() {
        this.validaUsuario = false;
        this.validaSenha = false;
        if (this.login.usuario !== '' && this.login.senha !== '') {
            return this.loginService.autenticarAPI(this.login).
                subscribe(
                    (data) => {
                        if (data.success) {
                            this.loginService.autenticado = true;
                            this.router.navigate(['produtos']);
                        } else {
                            this.loginService.autenticado = false;
                            this.erro = data.error;
                        }
                    },
                    error => {
                        console.log('Error: ', error);
                    });
        }
        else {
            if (this.login.usuario == '') {
                this.validaUsuario = true;
            }
            if (this.login.senha  == '') {
                this.validaSenha = true;
            }
        }
  }

}
