export interface Login {
  usuario: string;
  senha: string;
}

export interface LoginResposta {
  success: boolean;
  error: string;
}
