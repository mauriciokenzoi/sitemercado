export interface Produto {
  id: number;
  nome: string;
  valor: number;
  imagem: string;
}

export interface ProdutoResposta {
  success: boolean;
  msg: string;
}

