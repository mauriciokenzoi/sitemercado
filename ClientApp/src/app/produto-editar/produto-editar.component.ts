import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Produto } from '../models/Produto';
import { ApiProdutoService } from '../services/apiproduto.service';

@Component({
    selector: 'app-produto-editar',
    templateUrl: './produto-editar.component.html',
    styleUrls: ['./produto-editar.component.css']
})
export class ProdutoEditarComponent implements OnInit {
    @ViewChild('imgFile', { static: false }) inputFile: ElementRef;

    public produto: Produto;
    public carregando = true;
    public validaNome = false;
    public validaValor = false;
    public alterouImg = false;
    public msg = '';
    public caminhoImagem = null;

    constructor(private route: ActivatedRoute, private protudoService: ApiProdutoService, private router: Router) { }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.protudoService.buscarProduto(params.id).subscribe(
                (data) => {
                    this.produto = data;
                    if (this.produto.imagem !== null) {
                        this.caminhoImagem = '/img/produtos/' + this.produto.imagem;
                    }
                    this.carregando = false;
                },
                (erro) => {
                    console.log(erro);
                }
            );
        });
    }

    public salvar() {
        this.msg = '';
        if (this.produto.nome !== '' && this.produto.valor !== null) {
            if (this.alterouImg) {
                this.produto.imagem = this.caminhoImagem;
            }
            this.protudoService.editarProduto(this.produto).subscribe(
                (data) => {
                    this.msg = data.msg;
                    if (data.success) {
                        setTimeout(() => { this.router.navigate(['produtos']) }, 2200);
                    }
                },
                (erro) => {
                    console.log(erro);
                }
            );
        } else {
            if (this.produto.nome == '') {
                this.validaNome = true;
            }
            if (this.produto.valor == null) {
                this.validaValor = true;
            }
        }
    }

    processarImg(imageInput: any) {
        const img: File = imageInput.files[0];
        const reader = new FileReader();
        if (reader != null) {
            reader.addEventListener('load', (event: any) => {
                this.caminhoImagem = event.target.result;
                this.alterouImg = true;
            });
            reader.readAsDataURL(img);
        }
    }

    removerImg(imageInput: any) {
        this.caminhoImagem = null;
        this.produto.imagem = null;
        this.inputFile.nativeElement.value = "";
    }
}
