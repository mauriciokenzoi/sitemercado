import { Router } from '@angular/router';
import { Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { ApiProdutoService } from '../services/apiproduto.service';
import { Produto } from './../models/Produto';
​
@Component({
    selector: 'app-produto-novo',
    templateUrl: './produto-novo.component.html',
    styleUrls: ['./produto-novo.component.css']
})
export class ProdutoNovoComponent implements OnInit {​
    @ViewChild('imgFile', { static: false }) inputFile: ElementRef;
​
    public produto: Produto = { id: 0, nome: '', valor: null, imagem: null };
    public validaNome = false;
    public validaValor = false;
    public msg = '';
​
    constructor(private protudoService: ApiProdutoService, private router: Router) { }
​
    ngOnInit() {
    }
​
    public salvar() {
        this.msg = '';
        this.validaNome = false;
        this.validaValor = false;
        if (this.produto.nome !== '' && this.produto.valor !== null) {
            this.protudoService.inserirProduto(this.produto).subscribe(
                (data) => {
                    this.msg = data.msg;
                    if (data.success) {
                        setTimeout(() => { this.router.navigate(['produtos']) }, 2200);
                    }
                },
                (erro) => {
                    console.log(erro);
                }
            );
        } else {
            if (this.produto.nome == '') {
                this.validaNome = true;
            }
            if (this.produto.valor == null) {
                this.validaValor = true;
            }
        }
    }
​
    processarImg(imageInput: any) {
        const img: File = imageInput.files[0];
        const reader = new FileReader();
        if (reader != null) {
            reader.addEventListener('load', (event: any) => {
                this.produto.imagem = event.target.result;
            });
            reader.readAsDataURL(img);
        }
    }
​
    removerImg() {
        this.produto.imagem = null;
        this.inputFile.nativeElement.value = "";
    }
}
