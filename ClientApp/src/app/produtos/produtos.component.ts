import { Component, OnInit } from '@angular/core';
import { Produto } from '../models/Produto';
import { ApiProdutoService } from '../services/apiproduto.service';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})
export class ProdutosComponent implements OnInit {
  public carregando = true;
  public semRegistro = false;
  public produtos: Array<Produto>;
  public msg = '';

  constructor(private protudoService: ApiProdutoService) { }

  ngOnInit() {
    this.buscar();
  }

  public buscar() {
    this.protudoService.buscarProdutos().
    subscribe(
      (data) => {
        this.produtos = data;
        if (this.produtos.length === 0) {
          this.semRegistro = true;
        }
        this.carregando = false;
      },
      (erro) => {
          console.log('Error: ', erro);
      });
  }

  public Deletar(id) {
    this.protudoService.deletarProduto(id).subscribe(
      (data) => {
        if (data.success) {
          this.msg = data.msg;
          this.buscar();
        } else {
          this.msg = data.msg;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
