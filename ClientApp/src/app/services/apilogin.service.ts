import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Login, LoginResposta } from './../models/Login';

@Injectable()
export class ApiLoginService {
  url = environment.apiLogin;
  public autenticado: boolean;

  constructor(private http: HttpClient, private router: Router) { }

  public logado() {
    return this.autenticado;
  }

  public autenticarAPI(login: Login) {
    const auth = 'Basic ' + btoa(login.usuario + ':' + login.senha);
    const body = {};
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': auth
      })
    };

    return this.http.post<LoginResposta>(this.url, body, httpOptions);
  }

  public logout() {
    this.autenticado = false;
    this.router.navigate(['/autenticar']);
  }

}
