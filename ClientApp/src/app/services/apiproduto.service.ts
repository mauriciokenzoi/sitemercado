import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Produto, ProdutoResposta } from './../models/Produto';

@Injectable()
export class ApiProdutoService {

  urlBase = environment.apiProduto;
   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  body = {};

  constructor(private http: HttpClient) { }

  public produtos: Array<Produto>;

  public buscarProdutos() {
    return this.http.get<Array<Produto>>(this.urlBase, this.httpOptions);
  }

  public  buscarProduto(id: string) {
    const url = this.urlBase + '/' + id;
    return this.http.get<Produto>(url, this.httpOptions);
  }

  public  deletarProduto(id: string) {
    const url = this.urlBase + '/' + id;
    return this.http.delete<ProdutoResposta>(url, this.httpOptions);
  }

  public  inserirProduto(produto: Produto) {
    this.body = produto;
    return this.http.post<ProdutoResposta>(this.urlBase, this.body, this.httpOptions);
  }

  public  editarProduto(produto: Produto) {
    const url = this.urlBase + '/' + produto.id;
    this.body = produto;
    return this.http.put<ProdutoResposta>(url, this.body, this.httpOptions);
  }
}
