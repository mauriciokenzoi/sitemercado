import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { ApiLoginService } from './apilogin.service';

@Injectable()
export class AutenticacaoService implements CanActivate {

  constructor(private loginService: ApiLoginService, private router: Router) {}

  canActivate(): boolean {
    if (!this.loginService.logado()) {
        this.router.navigate(['autenticar']);
    } else {
      return true;
    }
  }
}
