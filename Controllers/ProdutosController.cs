﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SiteMercado.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMercado.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutosController : ControllerBase
    {
        private readonly SiteMercadoContext _context;
        private readonly LocalUpload _localupload;

        public ProdutosController(SiteMercadoContext context, IOptions<LocalUpload> localupload)
        {
            _context = context;
            _localupload = localupload.Value;
        }

        // GET: api/Produtos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Produto>>> GetProdutos()
        {
            return await _context.Produtos.ToListAsync();
        }

        // GET: api/Produtos/Nome=nome&ValorInicial=valor&ValorFinal=valor
        [HttpGet("GetByFilter")]
        public async Task<ActionResult<IEnumerable<Produto>>> GetProdutos(string Nome, decimal? ValorInicial, decimal? ValorFinal)
        {
            var lstPrd = (await _context.Produtos.ToListAsync());
            
            if (!string.IsNullOrEmpty(Nome))
                lstPrd = lstPrd.Where(x => x.Nome.Contains(Nome)).ToList();
            
            if (ValorInicial != null && ValorFinal != null)
            {
                if (ValorInicial < ValorFinal && ValorInicial != ValorFinal)
                {
                    lstPrd = lstPrd.Where(x => x.Valor >= ValorInicial.GetValueOrDefault() 
                                            && x.Valor <= ValorFinal.GetValueOrDefault()).ToList();
                }
                else if (ValorInicial > 0)
                {
                    lstPrd = lstPrd.Where(x => x.Valor >= ValorInicial.GetValueOrDefault()).ToList();
                }
                else if (ValorFinal > 0)
                {
                    lstPrd = lstPrd.Where(x => x.Valor <= ValorFinal.GetValueOrDefault()).ToList();
                }
            }

            return lstPrd;
        }

        // GET: api/Produtos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Produto>> GetProduto(int id)
        {
            var produto = await _context.Produtos.FindAsync(id);

            if (produto == null)
            {
                return NotFound(new { success = false, msg = "Produto não encontrado!" });
            }

            return produto;
        }

        // PUT: api/Produtos/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduto(int id, Produto produto)
        {
            if (id != produto.Id)
            {
                return BadRequest();
            }

            var foto = await _context.Produtos.FindAsync(id);
            _context.Entry(foto).State = EntityState.Detached;
            if (foto != null)
            {
                if (foto.Imagem != null && produto.Imagem == null)
                {
                    DeleteImg(foto);
                }
                else if(foto.Imagem == null && produto.Imagem != null)
                {
                    AddImg(produto);
                }
                else if (foto.Imagem != null && produto.Imagem != null && produto.Imagem != foto.Imagem)
                {
                    DeleteImg(foto);
                    AddImg(produto);
                }
            }
            else
            {
                return NotFound(new { success = false, msg = "Produto não encontrado!" });
            }

            _context.Entry(produto).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutoExists(id))
                {
                    return NotFound(new { success = false, msg = "Produto não encontrado!" });
                }
                else
                {
                    throw;
                }
            }

            return Ok(new { produto, success = true, msg = "Produto alterado com sucesso!" });
        }

        // POST: api/Produtos
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Produto>> PostProduto(Produto produto)
        {
            AddImg(produto);
            _context.Produtos.Add(produto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduto", new { id = produto.Id }, new { produto, success = true, msg = "Produto criado com sucesso!" });
        }

        // DELETE: api/Produtos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Produto>> DeleteProduto(int id)
        {
            var produto = await _context.Produtos.FindAsync(id);
            if (produto == null)
            {
                return NotFound(new { success = false, msg = "Produto não encontrado!" });
            }
            DeleteImg(produto);
            _context.Produtos.Remove(produto);
            await _context.SaveChangesAsync();

            return Ok(new { produto, success = true, msg = "Produto deletado com sucesso!" });
        }

        private bool ProdutoExists(int id)
        {
            return _context.Produtos.Any(e => e.Id == id);
        }
        
        private void AddImg(Produto produto)
        {
            if (produto.Imagem != null)
            {
                produto.Imagem = produto.Imagem.Replace("data:image/jpeg;base64,", "");
                produto.Imagem = produto.Imagem.Replace("data:image/jpg;base64,", "");
                if (produto.Imagem.Length > 0)
                {
                    string pasta = Path.Combine(Directory.GetCurrentDirectory(), _localupload.Img);

                    if (!Directory.Exists(pasta))
                    {
                        Directory.CreateDirectory(pasta);
                    }

                    string file = String.Format("Produto_{0}.jpg", DateTime.Now.Ticks);
                    using (var stream = new FileStream(pasta + file, FileMode.Create))
                    {
                        byte[] lst = Convert.FromBase64String(produto.Imagem);
                        for (int i = 0; i < lst.Length; i++)
                            stream.WriteByte(lst[i]);
                    }
                    produto.Imagem = file;
                }
            }
            else
            {
                produto.Imagem = null;
            }
        }

        private void DeleteImg(Produto produto) 
        {
            if (!string.IsNullOrEmpty(produto.Imagem))
            {
                string pasta = Path.Combine(Directory.GetCurrentDirectory(), _localupload.Img);
                string caminho = pasta + produto.Imagem;
                if (Directory.Exists(pasta) && System.IO.File.Exists(caminho))
                {
                    System.IO.File.Delete(caminho);
                }
            }
        }
    }
}
