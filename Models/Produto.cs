﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteMercado.Models
{
    public class Produto
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string Nome { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal Valor { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Imagem { get; set; }
    }
}
