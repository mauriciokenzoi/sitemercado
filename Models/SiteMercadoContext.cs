﻿using Microsoft.EntityFrameworkCore;

namespace SiteMercado.Models
{
    public class SiteMercadoContext : DbContext
    {
        public SiteMercadoContext(DbContextOptions<SiteMercadoContext> options) : base(options) { }

        public DbSet<Produto> Produtos { get; set; }
    }
}
