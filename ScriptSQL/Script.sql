CREATE DATABASE SiteMercado;

USE SiteMercado;

CREATE TABLE Produto(
Id INT NOT NULL PRIMARY KEY identity(1,1),
Nome VARCHAR(MAX) NOT NULL,
Valor DECIMAL(18,2) NOT NULL,
Imagem VARCHAR(MAX)
);